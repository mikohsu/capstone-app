// slider function
function updateSlider(slideAmount) {
    var sliderDiv = document.getElementById("complexRangeChoice");
    sliderDiv.innerHTML = slideAmount;
};

$(document).ready(() => {
  $('#sb1').addClass('active display')
  var $standardDiv = $('.standard-question');
  var $lastQuestion = $('.last-question');
  var $parentQuestion = $('.parent-question');
  var $sideBar =  $('.sb');
  var div_index = 1;
  var prev_div_index = div_index - 1;
    
  // next button
  $('#next-button').on('click', event => {
    var currDiv = $standardDiv.get(div_index);
    var prevDiv = $standardDiv.get(prev_div_index);
    var currSideBar  =  $sideBar.get(div_index);
    $(currDiv).show();
    $(currSideBar).addClass('display')
    // console.log($(currSideBar).attr('class'))
    $(currDiv).siblings().hide();
    $(currDiv).siblings().has('#next-button').show();
                
      if ($(currDiv).hasClass('.last-question')) {
          $(currDiv).siblings().has('.submit-button').show();
      }

    div_index++;
    $(prevDiv).hide()
    $( '.child-question' ).hide()
    prev_div_index++;

      // console.log(1)
      // console.log($(currDiv).attr('class'))
      // console.log($(currSideBar).attr('id'))
        
      // syncing the next-button to the side menu bar
    if($(currDiv).hasClass($(currSideBar).attr('id'))) {
      // console.log(2)
      
      $(currSideBar).addClass('active')
      $(currSideBar).siblings().removeClass('active')
        
    }

    // side bar navigation to previous questions
    $('.display').on('click', event => {  
      $(event.currentTarget).addClass('active')
      $(event.currentTarget).siblings().removeClass('active')
      
      var displayed_id = $(event.currentTarget).attr("id")
    
      $standardDiv.each(function() {
        if($( this ).hasClass(displayed_id)){
            console.log($(this))
            $(this).show()
            $(this).siblings().hide();
            $(this).siblings().has('#next-button').show();
            
        }  else if ($(this).hasClass('.last-question')) {
            $(this).siblings().has('.submit-button').show();
        }

      });   
    });

    // submit button
    if ($(currDiv).attr('id') === $lastQuestion.attr('id')) {
        $('#next-button').html("SUBMIT")
        $('#next-button').attr('type', 'submit');
        $('#next-button').addClass('submit-button');
    }

    $('.submit-button').on('click', () => {
        $('.submit-button').hide()
    })

    // clear form onClick submit button
    function clearInput() {
        document.querySelector('.standard-question').value=null;
        document.querySelector('.child-question').value=null;
    };

    $('.submit-button').on('click', () => {
        clearInput();
    })

  });

    // toggle child question onClick parent question
  $parentQuestion.on('click', event => {
    var parent_id = $(event.currentTarget).attr("id")
    console.log(parent_id)
    $( '.child-question' ).each(function() {
      if($( this ).hasClass(parent_id)){
        console.log($(this))
        $(this).show()
      } else if ($( this ).hasClass(parent_id) === false || $( this ).has('selected') === false) {
        $(this).hide()
      }
    });
  });


  // radio selections
  $('.section2').click(function() {
      $('.section2').not(this).prop('checked', false);
  });
  $('.section3').click(function() {
      $('.section3').not(this).prop('checked', false);
  });
  $('.section4').click(function() {
      $('.section4').not(this).prop('checked', false);
  });
  $('.section5').click(function() {
      $('.section5').not(this).prop('checked', false);
  });
  $('.section6').click(function() {
      $('.section6').not(this).prop('checked', false);
  });
  $('.section7').click(function() {
      $('.section7').not(this).prop('checked', false);
  });
  $('.section8').click(function() {
      $('.section8').not(this).prop('checked', false);
  });
  $('.section9').click(function() {
      $('.section9').not(this).prop('checked', false);
  });
  $('.section10').click(function() {
      $('.section10').not(this).prop('checked', false);
  });

  // popup text in confirmation.html
  $('#appointment-button').popover();

});


// calculate function
const availableOptions = {
  cb1: {
  hours: 20,
  price: 800,
  maxPrice: 1000,
  maxHours: 25
  },
  cb2: {
  hours: 24,
  price: 960,
  maxPrice: 1160,
  maxHours: 29
  },
  cb3: {
  hours: 10,
  price: 400,
  maxPrice: 600,
  maxHours: 15
  },
  cb4: {
  hours: 15,
  price: 600,
  maxPrice: 800,
  maxHours: 20
  },
  cb5: {
  hours: 20,
  price: 800,
  maxPrice: 1000,
  maxHours: 25
  },
  cb6: {
  hours: 10,
  price: 400,
  maxPrice: 600,
  maxHours: 15
  },
  cb7: {
  hours: 20,
  price: 800,
  maxPrice: 1000,
  maxHours: 25
  },
  cb8: {
  hours: 15,
  price: 600,
  maxPrice: 800,
  maxHours: 20
  },
  cb9: {
  hours: 25,
  price: 1000,
  maxPrice: 1200,
  maxHours: 30
  },
  cb10: {
  hours: 140,
  price: 5600,
  maxPrice: 5800,
  maxHours: 145
  },
  cb11: {
  hours: 120,
  price: 4800,
  maxPrice: 5000,
  maxHours: 125
  },
  cb12: {
  hours: 260,
  price: 10400,
  maxPrice: 10600,
  maxHours: 265
  },
  cb13: {
  hours: 40,
  price: 1600,
  maxPrice: 1800,
  maxHours: 45
  },
  cb14: {
  hours: 60,
  price: 2400,
  maxPrice: 2600,
  maxHours: 65
  },
  cb15: {
  hours: 120,
  price: 4800,
  maxPrice: 5000,
  maxHours: 125
  },
  cb16: {
  hours: 0,
  price: 0,
  maxPrice: 0,
  maxHours: 0
  },
  cb17: {
  hours: 0,
  price: 0,
  maxPrice: 0,
  maxHours: 0
  },
  cb18: {
  hours: 40,
  price: 1600,
  maxPrice: 1800,
  maxHours: 45
  },
  cb19: {
  hours: 25,
  price: 1000,
  maxPrice: 1200,
  maxHours: 30
  },
  cb20: {
  hours: 25,
  price: 1000,
  maxPrice: 1200,
  maxHours: 30
  },
  cb21: {
  hours: 25,
  price: 1000,
  maxPrice: 1200,
  maxHours: 30
  },
  cb22: {
  hours: 50,
  price: 2000,
  maxPrice: 2200,
  maxHours: 55
  },
  cb23: {
  hours: 50,
  price: 2000,
  maxPrice: 2200,
  maxHours: 55
  },
  cb24: {
  hours: 50,
  price: 2000,
  maxPrice: 2200,
  maxHours: 55
  },
  cb25: {
  hours: 50,
  price: 2000,
  maxPrice: 2200,
  maxHours: 55
  },
  cb26: {
  hours: 60,
  price: 2400,
  maxPrice: 2600,
  maxHours: 65
  },
  cb27: {
  hours: 75,
  price: 3000,
  maxPrice: 3200,
  maxHours: 80
  },
  cb28: {
  hours: 40,
  price: 1600,
  maxPrice: 1800,
  maxHours: 45
  },
  cb29: {
  hours: 40,
  price: 1600,
  maxPrice: 1800,
  maxHours: 45
  },
  cb30: {
  hours: 40,
  price: 1600,
  maxPrice: 1800,
  maxHours: 45
  },
  cb31: {
  hours: 0,
  price: 0,
  maxPrice: 0,
  maxHours: 0
  },
  cb32: {
  hours: 15,
  price: 600,
  maxPrice: 800,
  maxHours: 20
  },
  cb33: {
  hours: 50,
  price: 2000,
  maxPrice: 2200,
  maxHours: 55
  },
  cb34: {
  hours: 75,
  price: 3000,
  maxPrice: 3200,
  maxHours: 80
  },
  cb35: {
  hours: 50,
  price: 2000,
  maxPrice: 2200,
  maxHours: 55
  },
  cb36: {
  hours: 60,
  price: 2400,
  maxPrice: 2600,
  maxHours: 65
  },
  cb37: {
  hours: 60,
  price: 2400,
  maxPrice: 2600,
  maxHours: 65
  },
  cb38: {
  hours: 40,
  price: 1600,
  maxPrice: 1800,
  maxHours: 45
  },
  cb39: {
  hours: 40,
  price: 1600,
  maxPrice: 1800,
  maxHours: 45
  },
  cb40: {
  hours: 40,
  price: 1600,
  maxPrice: 1800,
  maxHours: 45
  },
  cb41: {
  hours: 50,
  price: 2000,
  maxPrice: 2200,
  maxHours: 55
  },
  cb42: {
  hours: 50,
  price: 2000,
  maxPrice: 2200,
  maxHours: 55
  },
  cb43: {
  hours: 40,
  price: 1600,
  maxPrice: 1800,
  maxHours: 45
  },
  cb44: {
  hours: 50,
  price: 2000,
  maxPrice: 2200,
  maxHours: 55
  },
  cb45: {
  hours: 15,
  price: 600,
  maxPrice: 800,
  maxHours: 20
  },
  cb46: {
  hours: 0,
  price: 0,
  maxPrice: 0,
  maxHours: 0
  }

};

let selectedOptions = {};
let totalPrice = 0;
let totalHours = 0;
let maxTotalPrice = 0;
let maxTotalHours = 0;


function calculate(clicked_id)  {
      let id_value = (clicked_id);
      let isRadioButton = document.getElementById(id_value).classList.contains('radio');
      if (isRadioButton) {
        singleChoiceCalculation(id_value);
      } else {
        multipleChoicesCalculation(id_value);
      }
      console.log(selectedOptions);

      updatePrice(totalPrice, maxTotalPrice);
      updateHours(totalHours, maxTotalHours);
};

function updateHours (totalHours, maxTotalHours) {
     document.getElementById('minHours').innerHTML = totalHours;
     document.getElementById('maxHours').innerHTML = maxTotalHours;
};

function updatePrice (totalPrice, maxTotalPrice) {
     document.getElementById('minPrice').innerHTML = totalPrice;
     document.getElementById('maxPrice').innerHTML = maxTotalPrice;
     document.getElementById('minCosts').innerHTML = totalPrice;
     document.getElementById('maxCosts').innerHTML = maxTotalPrice;
};

function singleChoiceCalculation(id_value) {
  let element = document.getElementById(id_value);
  let elementName = element.name;
  // check if a record already exists in the selectedOptions
  if (selectedOptions[elementName] && selectedOptions[elementName].price ) {
    // now we know there is an old price we have to change
    // remove the price from the total price
    totalPrice -= selectedOptions[elementName].price;
    totalHours -= selectedOptions[elementName].hours;
    maxTotalPrice -= selectedOptions[elementName].maxPrice;
    maxTotalHours -= selectedOptions[elementName].maxHours;
  }
  // check if same card was clicked (deselected)
  if (selectedOptions[elementName] && id_value == selectedOptions[elementName].id_value ) {
    delete selectedOptions[elementName];
  } else {
    // replace the old object with new selection in the selectedOptions
    selectedOptions[elementName] = availableOptions[id_value];
    // add id to help check for duplicate clicks
    selectedOptions[elementName].id_value = id_value;
    // set value
    selectedOptions[elementName].value = element.value;
    // add new price and hours
    totalPrice += selectedOptions[elementName].price;
    totalHours += selectedOptions[elementName].hours;
    maxTotalPrice += selectedOptions[elementName].maxPrice;
    maxTotalHours += selectedOptions[elementName].maxHours;
  }
};


function multipleChoicesCalculation(id_value){
  let element = document.getElementById(id_value);
  let elementName = element.name;
  let price = availableOptions[id_value].price;
  let hours = availableOptions[id_value].hours;
  let maxPrice = availableOptions[id_value].maxPrice;
  let maxHours = availableOptions[id_value].maxHours;
  // let sectionName = elementName + "_" + id_value;
  let sectionName = elementName;
  if(element.checked == true){
    if (!selectedOptions[sectionName]) { selectedOptions[sectionName] = [] }
    // selectedOptions[sectionName] = availableOptions[id_value];
    // selectedOptions[sectionName].id_value = id_value;
    selectedOptions[sectionName].push(element.value);
    totalPrice += price;
    totalHours += hours;
    maxTotalPrice += maxPrice
    maxTotalHours += maxHours
  } else {
    let index = selectedOptions[sectionName].findIndex(x => x == element.value);
    selectedOptions[sectionName].splice(index,1);
    totalPrice -= price;
    totalHours -= hours;
    maxTotalPrice -= maxPrice
    maxTotalHours -= maxHours
  }
};

// firebase


if (document.getElementById('summary-button')) {
  document.getElementById('summary-button').addEventListener('click', submitForm);
}

function getEmailInput(id){
  email =  document.getElementById(id).value;
  localStorage.setItem("email", email);
};


async function postToFirebase(obj){

  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyDHpot2GOtC1-Qm_3X1bwKDRP8yo21044k",
    authDomain: "cost-estimate-wit.firebaseapp.com",
    databaseURL: "https://cost-estimate-wit.firebaseio.com",
    projectId: "cost-estimate-wit",
    storageBucket: "cost-estimate-wit.appspot.com",
    messagingSenderId: "437236559063",
    appId: "1:437236559063:web:a3d2740109fd5890cf1492"
  };
  // Initialize Firebase
  if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
  }

  var ref = firebase.database().ref('form');

  console.log(obj);
  // ref.push().set({
  //     email: obj.email,
  //     additional_features: obj.additional_features,
  //     analytics_setup: obj.analytics_setup,
  //     backend_design: obj.backend_design,
  //     data_storage: obj.data_storage,
  //     device_support: obj.device_support,
  //     estimated_max_hours: obj.estimated_max_hours,
  //     estimated_max_price: obj.estimated_max_price,
  //     estimated_min_hours: obj.estimated_min_hours,
  //     estimated_min_price: obj.estimated_min_price,
  //     frontend_design: obj.frontend_design,
  //     general_features: obj.general_features,
  //     login_features: obj.login_features,
  //     login_options: obj.login_options,
  //     media_features: obj.media_features,
  //     multilingual_support: obj.multilingual_support,
  //     platform: obj.platform,
  //     screensetting_level: obj.screensetting_level,
  //     store_data_option: obj.store_data_option,
  //     third_party_services: obj.third_party_services,
  //     utility_features: obj.utility_features,
  //     ux_features: obj.ux_features
  //   }, function(error) {
  //     if (error) {
  //       alert("error")
  //     } else {
  //       location.href="./confirmation.html";
  //     }
  //   });
    ref.push().set(obj, function(error) {
        if (error) {
          alert("error")
        } else {
          location.href="./confirmation.html";
        }
    });
};

function submitForm (event){
  let userEmail = localStorage.getItem("email");
  console.log(userEmail);
  event.preventDefault();
  var obj = {
    email: "",
    additional_features: "",
    analytics_setup: "",
    backend_design: "",
    data_storage: "",
    device_support: "",
    estimated_max_hours: "",
    estimated_max_price: "",
    estimated_min_hours: "",
    estimated_min_price: "",
    frontend_design: "",
    general_features: "",
    login_features: "",
    login_options: "",
    media_features: "",
    multilingual_support: "",
    platform: "",
    screensetting_level: "",
    store_data_option: "",
    third_party_services: "",
    utility_features: "",
    ux_features: "",
    complexity_level: "",
    languages: "",
    date: "",
    date_iso:""
  }

  console.log(selectedOptions);
  for (property in selectedOptions) {
    if (Array.isArray(selectedOptions[property])) {
      obj[property] = JSON.stringify(selectedOptions[property]);
    } else {
      try {
        obj[property] = selectedOptions[property].value;
      } catch {
        console.log('No value for ' + selectedOptions[property].name);
      }
    }
  }
  obj['email'] = userEmail;
  console.log("VALUE / / / / / / / / / / / / / / : \n" + new Date());
  obj['languages'] = document.getElementById("num").value;
  obj['complexity_level'] = document.getElementById("complexRangeChoice").innerHTML;
  obj['date'] = new Date().toDateString();
  obj['date_iso'] = new Date().toISOString();
  obj['estimated_max_hours'] = maxTotalHours;
  obj['estimated_max_price'] = maxTotalPrice;
  obj['estimated_min_hours'] = totalHours;
  obj['estimated_min_price'] = totalPrice;

  postToFirebase(obj);

}


/*header slider */


const nextBtn = document.getElementById('next-button');
const content = document.getElementById('content');
const bullets = document.querySelectorAll('.bullet');

const MAX_STEPS = 14;
let currentStep = 1;

nextBtn.addEventListener('click', () => {
  bullets[currentStep - 1].classList.add('completed');
  currentStep += 1;

 //  if (currentStep === MAX_STEPS) {
 //    nextBtn.disabled = true;
 // }


  // content.innerText = `Complete Step ${currentStep}`;
});


// $("#slider-container").hide();
//     $("#slider-container").css({'display': 'none'});

// // email-confirm
// console.log('THE EMAIL IS : ' + localStorage.getItem("email"));
// document.getElementById("email-confirm").innerHTML = localStorage.getItem("email");
